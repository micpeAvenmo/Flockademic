jest.mock('../../src/services/periodical', () => ({
  getDashboard: jest.fn().mockReturnValue(Promise.resolve(
    {
      articles: [
        { identifier: 'first', name: 'One article' },
        { identifier: 'second', name: 'Another article' },
      ],
      periodicals: [
        { identifier: 'first', name: 'One periodical' },
        { identifier: 'second', name: 'Another periodical' },
      ],
    },
  )),
}));
jest.mock('../../src/services/account', () => ({
  getSession: jest.fn().mockReturnValue(Promise.resolve({ identifier: 'arbitrary user id' })),
}));

import { DashboardPage } from '../../src/components/dashboardPage/component';
import { Spinner } from '../../src/components/spinner/component';
import { mockRouterContext } from '../mockRouterContext';

import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

function initialisePage() {
  return mount(
    <DashboardPage match={{ url: 'https://arbitrary_url' }}/>,
    mockRouterContext,
  );
}

it('should display a spinner while the dashboard contents are loading', () => {
  const page = initialisePage();

  expect(page.find(Spinner)).toExist();
});

it('should display an error when the dashboard contents could not be loaded', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getDashboard.mockReturnValueOnce(Promise.reject('Arbitrary error'));
  const page = initialisePage();

  setImmediate(checkAssertions);
  function checkAssertions() {
    page.update();
    expect(toJson(page.find(DashboardPage))).toMatchSnapshot();
    done();
  }
});

it('should display the dashboard contents when they have been loaded', (done) => {
  const page = initialisePage();

  setImmediate(checkAssertions);
  function checkAssertions() {
    page.update();
    expect(toJson(page.find(DashboardPage))).toMatchSnapshot();
    done();
  }
});

it('should not list articles when there was an error loading them', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getDashboard.mockReturnValueOnce(Promise.resolve({
    articles: null,
  }));
  const page = initialisePage();

  setImmediate(checkAssertions);
  function checkAssertions() {
    page.update();
    expect(page.find('[data-test-id="article-list"]')).not.toExist();
    done();
  }
});

it('should not list journals when there was an error loading them', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getDashboard.mockReturnValueOnce(Promise.resolve({
    periodicals: null,
  }));
  const page = initialisePage();

  setImmediate(checkAssertions);
  function checkAssertions() {
    page.update();
    expect(page.find('[data-test-id="journal-list"]')).not.toExist();
    done();
  }
});

it('should suggest to submit an article when the user has not submitted any yet', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getDashboard.mockReturnValueOnce(Promise.resolve({
    articles: [],
  }));
  const page = initialisePage();

  setImmediate(checkAssertions);
  function checkAssertions() {
    page.update();
    expect(page.find('Link[to="/articles/new"]')).toExist();
    done();
  }
});

it('should suggest to start a journal when the user manages no journals', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getDashboard.mockReturnValueOnce(Promise.resolve({
    periodicals: [],
  }));
  const page = initialisePage();

  setImmediate(checkAssertions);
  function checkAssertions() {
    page.update();
    expect(page.find('Link[to="/journals/new"]')).toExist();
    done();
  }
});
