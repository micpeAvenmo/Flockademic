jest.mock('../../src/services/isPeriodicalSlugAvailable', () => ({
  isPeriodicalSlugAvailable: jest.fn().mockReturnValue(Promise.resolve(true)),
}));

import { isPeriodicalSlugAvailable } from '../../src/resources/isPeriodicalSlugAvailable';

const mockContext = {
  database: {} as any,

  body: undefined,
  headers: {},
  method: 'GET' as 'GET',
  params: [ '/arbitrary_id/exists', 'arbitrary_id' ],
  path: '/arbitrary_id/exists',
  query: null,
};

it('should error when no Journal ID was specified', () => {
  const promise = isPeriodicalSlugAvailable({ ...mockContext, params: [], path: '/' });

  return expect(promise).rejects.toEqual(new Error('Could not find a journal without a journal ID.'));
});

it('should error when the service errors', () => {
  const mockedService = require.requireMock('../../src/services/isPeriodicalSlugAvailable');
  mockedService.isPeriodicalSlugAvailable.mockReturnValueOnce(Promise.reject('Arbitrary error'));

  const promise = isPeriodicalSlugAvailable(mockContext);

  return expect(promise).rejects.toEqual(new Error('There was a problem checking for the journal\'s existence.'));
});

it('should report it when no periodical could be found with the slug', () => {
  const mockedService = require.requireMock('../../src/services/isPeriodicalSlugAvailable');
  mockedService.isPeriodicalSlugAvailable.mockReturnValueOnce(Promise.resolve(true));

  const promise = isPeriodicalSlugAvailable(mockContext);

  return expect(promise).resolves.toEqual({
    error: { description: 'No existing periodical found with that name.' },
  });
});

it('should report it when there exists a periodical with the given slug', () => {
  const mockedService = require.requireMock('../../src/services/isPeriodicalSlugAvailable');
  mockedService.isPeriodicalSlugAvailable.mockReturnValueOnce(Promise.resolve(false));

  const promise = isPeriodicalSlugAvailable({
    ...mockContext,
    params: [ '/some_id/exists', 'some_id' ],
    path: '/some_id/exists',
  });

  return expect(promise).resolves.toEqual({
    result: { identifier: 'some_id' },
  });
});
