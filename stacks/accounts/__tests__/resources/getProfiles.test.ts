jest.mock('../../src/dao', () => ({ getProfilesForAccounts: jest.fn().mockReturnValue(Promise.resolve([])) }));

import { getProfiles } from '../../src/resources/getProfiles';

const mockContext = {
  body: {},
  database: {} as any,

  headers: {},
  method: 'GET' as 'GET',
  params: [ '/profiles/profile1+profile2', 'profile1+profile2' ],
  path: '/profiles/profile1+profile2',
  query: null,
};

it('should return the fetched profiles', () => {
  const mockedDao = require.requireMock('../../src/dao');
  mockedDao.getProfilesForAccounts.mockReturnValueOnce(Promise.resolve([
    { identifier: 'profile1', orcid: 'orcid1' },
    { identifier: 'profile2', orcid: 'orcid2', name: 'Some name' },
  ]));

  return expect(getProfiles(mockContext)).resolves.toEqual({
    profiles: [
      { identifier: 'profile1', sameAs: 'https://orcid.org/orcid1' },
      { identifier: 'profile2', sameAs: 'https://orcid.org/orcid2', name: 'Some name' },
    ],
  });
});

it('should fetch profiles for every identifier passed', () => {
  const mockedDao = require.requireMock('../../src/dao');

  getProfiles({
    ...mockContext,
    params: [ '/profiles/profile1+profile2', 'profile1+profile2' ],
    path: '/profiles/profile1+profile2',
  });

  expect(mockedDao.getProfilesForAccounts.mock.calls.length).toBe(1);
  expect(mockedDao.getProfilesForAccounts.mock.calls[0][1]).toEqual([ 'profile1', 'profile2' ]);
});

it('should error when no account identifiers were passed', () => {
  return expect(getProfiles({
    ...mockContext,
    params: [ '/profiles' ],
    path: '/profiles',
  })).rejects.toEqual(new Error('Cannot fetch profile data without account IDs.'));
});

it('should error when the profile data could not be fetched', () => {
  const mockedDao = require.requireMock('../../src/dao');
  mockedDao.getProfilesForAccounts.mockReturnValueOnce(Promise.reject('Arbitrary error'));

  return expect(getProfiles(mockContext)).rejects.toEqual(new Error('Could not fetch profile data.'));
});
